import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": "landmark " + city + " " + state,
        "per_page": 1
    }
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(conference):
    latlon_params = {
        'appid': OPEN_WEATHER_API_KEY,
        'q': {conference.location.city, conference.location.state, "US"}
        }

    latlon_url = 'http://api.openweathermap.org/geo/1.0/direct'
    latlon_response = requests.get(latlon_url, latlon_params)
    latlon_data = latlon_response.json()

    weather_params = {
        'appid': OPEN_WEATHER_API_KEY,
        }
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latlon_data[0]['lat']}&lon={latlon_data[0]['lon']}"
    weather_response = requests.get(weather_url, weather_params)
    weather_data = weather_response.json()
    print(weather_data)

    conf_weather = {
        "temp": weather_data['main']['temp'],
        "description": weather_data['weather'][0]['description'],
    }

    return conf_weather
